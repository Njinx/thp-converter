#!/bin/sh

if [ $# -ne 2 ]; then
    echo "./mp42thp INPUT_MP4 OUTPUT_THP"
    exit 1
fi

framerate=$(mediainfo --Output=JSON "$1" | jq -r '.media.track[0].FrameRate' | sed -e 's/^[0]*//' -e 's/[0]*$//g')

frames_tmp=$(mktemp -d)
audio_tmp=$(mktemp -d)

cleanup() {
    rm -rf "$frames_tmp"
    rm -rf "$audio_tmp"
}

trap cleanup EXIT

echo "Extracting video frames..."
ffmpeg -i "$1" -r "$framerate" -qscale:v 2 "${frames_tmp}/%06d.jpg"

echo "Extracting audio..."
mplayer -vo null -ao pcm:file="${audio_tmp}/temp.wav" "$1"
# ffmpeg -i "$1" -f s16 -acodec pcm_s16le "${audio_tmp}/temp.wav"

echo "Fixing bit depth..."
ffmpeg -i "${audio_tmp}/temp.wav" -sample_fmt s16 "${audio_tmp}/temp_16bit.wav"

echo "Converting to THP..."
wine "$(dirname "$(realpath "$0")")/thpconv/thpconv.exe" -j "${frames_tmp}/*.jpg" \
    -s "${audio_tmp}/temp_16bit.wav" -r "$framerate" -d "$2" -v
#!/bin/sh

if [ $# -ne 2 ]; then
    echo "./thp2mp4 INPUT_THP OUTPUT_MP4"
    exit 1
fi

ffmpeg -i "$1" "$2"
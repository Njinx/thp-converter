# THP Converter

Very messy set of scripts to convert between Nintendo's Gamecube/Wii THP format and MP4.

Requirements

* ffmpeg
* mediainfo
* GNU Coreutils
* jq
* mplayer
* wine
